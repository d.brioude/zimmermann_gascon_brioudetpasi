'use strict';
var express = require("express");
var http = require("http");
var path = require("path");
const bodyParser = require('body-parser');
var CONFIG = require("./config.json");
process.env.CONFIG = JSON.stringify(CONFIG);
// Acces à CONFIG dans les autres modules: var CONFIG = JSON.parse(process.env.CONFIG);

var app = express();
// beware, it should not be used in combination with the multer. Consider the method with chuncks instead.
app.use(bodyParser.json())

// init server
var server = http.createServer(app);
server.listen(CONFIG.port);

var defaultRoute = require("./app/routes/default.route.js");
var loadPres = require("./app/routes/loadPres.route.js");
var savePres = require("./app/routes/savePres.route.js");
var login = require("./app/routes/login.route.js");
var contentRouter = require("./app/routes/content.router");

var IOController = require("./app/controllers/io.controller.js");

app.use(defaultRoute);
app.use(loadPres);
app.use(savePres);
app.use(login);
app.use(contentRouter);

app.use("/admin", express.static(path.join(__dirname, "public/admin")));
app.use("/watch", express.static(path.join(__dirname, "public/watch")));
app.use("/frontmoke", express.static(path.join(__dirname, "public/frontmoke")));

//var io = require('socket.io')(http);
var io = require('socket.io').listen(server);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

IOController.listen(io);