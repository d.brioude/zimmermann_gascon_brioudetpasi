'use strict';
const utils = require("../utils/utils.js");
const fs = require("fs");
let CONFIG = JSON.parse(process.env.CONFIG);


// home/cpe/Documents/work/NodeJS/cpe-5IRC-2018/NodeJS/tp_starter
// /home/cpe/Documents/work/NodeJS/cpe-5IRC-2018/NodeJS/tp_starter

class ContentModel {
    constructor({type, id, title, src, fileName}={}){
        let data;
        this.type = type;  // public - ['img', 'img_url', 'video', 'web']
        this.id = id;  // public - UUID
        this.title = title;  // public
        this.src = src;  // public - l'URL qui permet d'acceder au contenu
        this.fileName = fileName;  // public - le nom du fichier stocké dans [CONFIG.contentDirectory] dans le cas d'un contenu de type 'img'. Il correspond a l'id du contenu + l'extension qui sera récupérée à partir du fichier original (png, jpeg...).

        this.getData = function(){
            return data;
        }

        this.setData = function(myData){
            data = myData;
        }
    }

    setId(){
        this.id = utils.generateUUID();
    }

    setSrc(){
        this.src = CONFIG["presentationDirectory"] + this.id;
    }

    static create(content, callback){
        // Prend un objet ContentModel en paramètre, stocke le contenu de [content.data] dans le fichier [content.fileName]
        // (dans le cas d'un contenu de type 'img') et stocke les meta-données dans un fichier [content.contentModel.id].meta.json
        // dans le répertoire [CONFIG.contentDirectory]. Les meta-données sont obtenus en faisant un JSON.parse(content)

        if (content == null){
            return callback(new Error("Null object received => Abort."));
        } else if (!(content instanceof ContentModel)){
            return callback(new Error("Unknown type of object received => Abort."));
        }
        else if (content.id == null){
            return callback(new Error("Null id received => Abort."));
        }
        else {
            fs.writeFile(CONFIG["contentDirectory"] + "/" + content.id + '.meta.json', JSON.stringify(content), function(err) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                console.log('Properly wrote meta to file ' + content.id + '.meta.json');

                if (content.type == "img" && content.getData() && content.getData().length > 0) {
                    fs.writeFile(CONFIG["contentDirectory"] + "/" + content.fileName, content.getData(), function(err) {
                        if(err) {
                            console.log(err);
                            return callback(err);
                        }
                        console.log('Properly wrote image in file '+ content.fileName);
                        callback();
                    });
                } else {
                    callback();
                }
            });
        }
    }

    static read(id, callback){
        // Prend un id en paramètre et retourne l'objet ContentModel lu depuis le fichier [id].meta.json
        if (id == null){
            return callback(new Error("Null id received => Abort."));
        } else {
            fs.readFile(CONFIG["contentDirectory"] + "/" + id + '.meta.json', function(err, content) {
                if (err) {
                    return callback(err, null);
                } else {
                    let theContent = new ContentModel(JSON.parse(content.toString()));
                    return callback(null, theContent);
                }
            });
        }
    }

    static update(content, callback){
        // Prend un objet ContentModel en paramètre et met à jour le fichier de metadata ([content.id].meta.json).
        // Le fichier [content.fileName] est mis à jour si [content.type] est égal à 'img' et si [content.data] est renseigné
        // (non nul avec une taille > 0).
        let present = false;

        if (content == null){
            return callback(new Error("Null object received => Abort."));
        } else if (!(content instanceof ContentModel)){
            return callback(new Error("Unknown type of object received => Abort."));
        }
        else if (content.id == null){
            return callback(new Error("Null id received => Abort."));
        }
        else {
            fs.readdir(CONFIG["contentDirectory"], (err, files) => {
                console.log("dir content");
                console.log(files.toString());
                console.log("look for file ");
                console.log(content.id + ".meta.json");
                if (files.toString().includes(content.id + ".meta.json" )) {
                    fs.writeFile(CONFIG["contentDirectory"] + "/" + content.id + '.meta.json', JSON.stringify(content), function(err) {
                        if (err) {
                            console.log(err);
                            return callback(err);
                        }
                        console.log('Properly wrote meta to file ' + content.id + '.meta.json');
                        if (content.type == "img" && content.getData().length > 0) {
                            fs.writeFile(CONFIG["contentDirectory"] + "/" + content.fileName, content.getData(), function(err) {
                                if(err) {
                                    console.log(err);
                                    return callback(err);
                                }
                                console.log('Properly wrote image in file '+ content.fileName);
                                return callback();
                            });
                        } else {
                            return callback();
                        }
                    });

                } else {
                    return callback(new Error("File to be updated doesn't exist => Abort."));
                }
            });
        }
    }


    static delete(id, callback){
        // Prend un id en paramètre et supprime les fichiers data ([content.fileName]) et metadata ([content.id].meta.json)

        fs.readFile(CONFIG["contentDirectory"] + "/" + id + '.meta.json', function(err, content) {
            if (err) {
                console.log(err);
                return callback(err);
            } else {

                content = new ContentModel(JSON.parse(content.toString()));

                fs.unlink(CONFIG["contentDirectory"] + "/" + id + ".meta.json", function(err) {
                    if (err) {
                        console.log("failed to delete local meta: "+err);
                        return callback(err);
                    } else {
                        if (content.type == "img") {
                            fs.unlink(CONFIG["contentDirectory"] + "/" + content.fileName, function(err) {
                                if (err) {
                                    console.log("failed to delete local image: "+err);
                                    return callback(err);
                                } else {
                                    return callback();
                                }
                            });
                        } else {
                            return callback();
                        }
                    }
                });
            }
        });
    }

    static list(callback){
        let presentations = {};
        fs.readdir(CONFIG["presentationDirectory"], (err, files) => {
            let idx = 0;
            files.forEach(file_name => {
                if (file_name.split(".")[2] == "json") {
                    let file_path = CONFIG["presentationDirectory"] + "/" + file_name;
                    fs.readFile(file_path, function(error, content) {
                        content = JSON.parse(content);
                        presentations[content.id] = content;
                        idx += 1;
                        if (idx == files.length){
                            callback(null, presentations);
                        }
                    });
                }
            });
        })
    }


    static treat(command, presId, slidId, room, callback){

        // console.log("DEBUG slidId " + slidId);
        // console.log("DEBUG presId " + presId);

        readPres(
            presId,
            (result) => {
                if(result.err != null){
                    console.log("ERRORR");
                    callback(result.err, null);
                }
                else if (result.presentation != null) {

                    if (command == "START"){
                        startCmd(result.presentation, slidId, room, callback);
                        // return callback(ret.get("err"), ret["metadata"]);
                    } else if (command == "PAUSE"){
                        pauseCmd(result.presentation, slidId, room, callback);
                    } else if (command == "END"){
                        endCmd(result.presentation, slidId, room, callback);
                    } else if (command == "BEGIN"){
                        beginCmd(result.presentation, slidId, room, callback);
                    } else if (command == "PREV"){
                        prevCmd(result.presentation, slidId, room, callback);
                    } else if (command == "NEXT"){
                        nextCmd(result.presentation, slidId, room, callback);
                    } else {
                        console.log("ERRORR");
                        callback("Unknown command.", null);
                    }
                }
            }
        );
    }
}
module.exports = ContentModel;

// to do gérer les erreurs (next en fin de list notamment)

function startCmd(presentation, slidId, room, callback){
    if (!slidId) {
        var presentation = JSON.parse(presentation);
        var newSlidId = presentation["slidArray"][0]["id"]; // cmd start = first slid = 0
        room["state"] = "start";
        room["slidId"] = newSlidId;
        readSlidMetaData(newSlidId, room, callback);
    } else {
        room["state"] = "start";
        room["slidId"] = slidId;
        readSlidMetaData(slidId, room, callback);
    }
}

function pauseCmd(presentation, slidId, room, callback){
    if (!slidId) {
        room["state"] = "pause";
        if (room["slidId"]) {
            readSlidMetaData(room["slidId"], room, callback);
        } else {
            readSlidMetaData(null, room, callback);
        }
    } else {
        room["state"] = "pause";
        room["slidId"] = slidId; // cmd pause = do nothing
        readSlidMetaData(slidId, room, callback);
    }
}

function endCmd(presentation, slidId,  room, callback){
    var presentation = JSON.parse(presentation);
    var newSlidId = presentation["slidArray"][-1]["id"]; // cmd end = last slid = -1
    room["slidId"] = newSlidId;
    readSlidMetaData(newSlidId, room, callback);
}

function beginCmd(presentation, slidId,  room, callback){
    var presentation = JSON.parse(presentation);
    var newSlidId = presentation["slidArray"][0]["id"]; // cmd begin = first slid = 0
    room["slidId"] = newSlidId;
    readSlidMetaData(newSlidId, room, callback);
}

function prevCmd(presentation, slidId,  room, callback){
    if (!slidId) {
        if (room["slidId"]) {
            var presentation = JSON.parse(presentation);
            presentation["slidArray"].forEach((slid, idx) => {
                if (slid["id"] == room["slidId"]){
                    if (presentation["slidArray"][idx+1]){
                        var newSlidId =  presentation["slidArray"][idx-1]["id"];
                        room["slidId"] = newSlidId;
                        readSlidMetaData(newSlidId, room, callback);
                    } else {
                        // nothing changes ?should we ret some err msg?
                        room["slidId"] = slidId;
                        readSlidMetaData(slidId, room, callback);
                    }
                }
            });
        } else {
            readSlidMetaData(null, room, callback);
        }
    } else {
        var presentation = JSON.parse(presentation);
            presentation["slidArray"].forEach((slid, idx) => {
                if (slid["id"] == slidId){
                    if (presentation["slidArray"][idx+1]){
                        var newSlidId =  presentation["slidArray"][idx-1]["id"];
                        room["slidId"] = newSlidId;
                        readSlidMetaData(newSlidId, room, callback);
                    } else {
                        // nothing changes ?should we ret some err msg?
                        room["slidId"] = slidId;
                        readSlidMetaData(slidId, room, callback);
                    }
                }
            });

    }
}

function nextCmd(presentation, slidId, room, callback){
    if (!slidId) {
        if (room["slidId"]) {
            var presentation = JSON.parse(presentation);
            presentation["slidArray"].forEach((slid, idx) => {
                if (slid["id"] == room["slidId"]){
                    if (presentation["slidArray"][idx+1]){
                        var newSlidId =  presentation["slidArray"][idx+1]["id"];
                        room["slidId"] = newSlidId;
                        readSlidMetaData(newSlidId, room, callback);
                    } else {
                        // nothing changes ?should we ret some err msg?
                        room["slidId"] = slidId;
                        readSlidMetaData(slidId, room, callback);
                    }
                }
            });
        } else {
            readSlidMetaData(null, room, callback);
        }
    } else {
        var presentation = JSON.parse(presentation);
            presentation["slidArray"].forEach((slid, idx) => {
                if (slid["id"] == slidId){
                    if (presentation["slidArray"][idx+1]){
                        var newSlidId =  presentation["slidArray"][idx+1]["id"];
                        room["slidId"] = newSlidId;
                        readSlidMetaData(newSlidId, room, callback);
                    } else {
                        // nothing changes ?should we ret some err msg?
                        room["slidId"] = slidId;
                        readSlidMetaData(slidId, room, callback);
                    }
                }
            });
    }
}

function readSlidMetaData(newSlidId, room, callback){
    fs.readFile(CONFIG["contentDirectory"] + "/"  + newSlidId + '.meta.json', function(err, slidMeta) {
        if (err) {
            // callback(err, null);
            callback(null, {id: newSlidId}, room);
        } else {
            callback(null, slidMeta, room);
        }
    });

}

function readPres(presId, callback){
    fs.readFile(CONFIG["presentationDirectory"] + "/"  + presId + '.pres.json', function(err, presentation) {
        if (err) {
            callback({err: err});
        } else {
            callback({presentation: presentation})
        }
    });
}
