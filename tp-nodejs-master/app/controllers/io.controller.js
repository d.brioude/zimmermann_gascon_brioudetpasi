var contentModel = require('../models/content.model');

class IOController {
    static listen(server) {
        console.log("listening...")

        var activeSockets = {}
        var activeRooms = {}

        var slidEvents = [
            "START",
            "PAUSE",
            "END",
            "BEGIN",
            "PREV",
            "NEXT"
        ]

        server.on('connection', function (socket) {
            console.log('Un client est connecté !');

            // var myTimer = setInterval(function() {waiter(socket)}, 10*1000);

            socket.on("data_comm", function (data){

                if (!activeRooms[data.presId]){
                    console.log("NEW ROOM ADDED");
                    // create and register a room
                    activeRooms[data.presId] = {};
                }

                function waiter(mySocket){
                    if (activeRooms[data.presId] && activeRooms[data.presId].state  == "start" && activeRooms[data.presId].slidId) {
                        console.log("");
                        console.log("5 sec passed call NEXT with " + activeRooms[data.presId].slidId);

                        contentModel.treat("NEXT", data.presId, activeRooms[data.presId].slidId, activeRooms[data.presId], (err, res, activeRoom) =>
                            {
                                if (err){
                                    console.log("An error occurred " + err);
                                    socket.emit("failure", {
                                        cmdId: "auto next",
                                        cmdType: "NEXT",
                                        presId: data.presId,
                                        slidId: activeRooms[data.presId].slidId,
                                        result: err
                                    });
                                } else if (res){
                                    var ret = {
                                            cmdId: "auto next",
                                            cmdType: "NEXT",
                                            presId: data.presId,
                                            slidId: res.id,
                                            // 'res' can sometimes only contain an id if there is no meta data = no content
                                            result: res // metadata that comes from above function 'treat'
                                        }
                                    console.log("AUTO RETURN " + JSON.stringify(ret));
                                    socket.emit("success", ret).to(data.presId);
                                   //  socket.emit("success", {result: {id: "LOLOL"}}).to(data.presId);
                                }
                                if (activeRoom) {
                                    activeRooms[data.presId] = activeRoom;
                                }
                            });

                    } else {
                        console.log("5 sec passed. Do nothing it's on pause.");
                    }
                }

                if (activeRooms[data.presId]["timer"]){
                } else {
                    activeRooms[data.presId]["timer"] = setInterval(function() {waiter(socket)}, 10*1000);
                }

                // the clientId seems to be useless. To be clarified
                activeSockets[data.clientId] = socket;

                // join a room
                this.join(data.presId);
                console.log("New client " + data.clientId + " just registered to room " + data.presId);

                this.on('slidEvent', function(data){
                    // console.log("Just received the following cmd: " + JSON.stringify(data))
                    var error;

                    if (slidEvents.includes(data.event.toUpperCase())) {
                        if (data.presId != null) {

                            // console.log("DEBUG ");
                            // console.log(data);

                            contentModel.treat(data.event, data.presId, data.slidId, activeRooms[data.presId], (err, res, activeRoom) =>
                            {
                                if (err){
                                    console.log("An error occurred " + err);
                                    error = err;
                                } else if (res){
                                    // console.log("cmd success");
                                    // console.log("");
                                    var ret = {
                                            cmdId: data.cmdId,
                                            cmdType: data.event,
                                            presId: data.presId,
                                            slidId: res.id,
                                            // 'res' can sometimes only contain an id if there is no meta data = no content
                                            result: res // metadata that comes from above function 'treat'
                                        }
                                    console.log("RETURN " + JSON.stringify(ret))
                                    // clearInterval(activeRooms[data.presId]["timer"]);
                                    // activeRooms[data.presId]["timer"] = setInterval(function() {waiter(socket)}, 10*1000);
                                    socket.emit("success", ret).to(data.presId);
                                }
                                if (activeRoom) {  // = if start or pause
                                    activeRooms[data.presId] = activeRoom;
                                }
                            });
                        }
                        else {
                            error = "Invalid presentation Id " + data.presId;
                        }
                    }
                    else {
                        error = "Unknown command type " + data.event;
                    }

                    if (error) {
                        console.log("ERROR: Client has been notify that an error occurred: " + error);
                        socket.emit("failure", {
                            cmdId: data.cmdId,
                            cmdType: data.event,
                            presId: data.presId,
                            slidId: data.slidId,
                            result: error
                        });
                    }

                });

                this.on('disconnect', function () {
                    delete activeSockets[data.clientId];
                    console.log("Client " + data.clientId + " just disconnected!");
                });
            });
            socket.emit("connection");  // kinda notifies the client that it is now connected
        });
    }
}

module.exports = IOController;
