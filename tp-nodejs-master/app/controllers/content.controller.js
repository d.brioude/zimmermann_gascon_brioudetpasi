var contentModel = require('../models/content.model');

class ContentController {
    static read(req,res) {
        let id = req.params.id;
        if (id){
            let boolJSON = req.param('json');
            contentModel.read(id,(err, data) => {
                if(err){
                    console.error(err);
                    res.status(500).end(err.message);
                }
                if(boolJSON !== 'true'){
                    if(data.fileName === null){
                        res.redirect(data.src);
                    }else{
                        console.log('renvoi de fichier');
                        res.sendFile(util.getDataFilePath (data.fileName));
                    }
                }else{
                    res.json(data);
                }
            });
        }
        else {
            res.json("Missing parameter 'id'.");
        }
    }

    static list(req, res) {
        contentModel.list((err, data) => {
            if(err){
                console.error(err);
                res.status(500).end(err.message);
            }
            if(data.length < 1){
                    res.json("No content found.");
            }else{
                console.log('renvoi de la list de fichiers');
                console.log(data);
                res.json(data);
            }
        });
    }

    static create(req, res) {
        let types = ['img', 'img_url', 'video', 'web'];

        if (req.body.type && req.body.id && req.body.title && req.body.fileName && req.body.src) {
            if (types.toString().includes(req.body.type)){

                let content = new contentModel({
                    'type': req.body.type,
                    'id': req.body.id,
                    'title': req.body.title,
                    'src': req.body.src,
                    'fileName': req.body.fileName
                });

                contentModel.create(content, (err) => {
                    if(err){
                        console.error(err);
                        res.status(500).end(err.message);
                    }else{
                        res.json("File properly created.");
                    }
                });
            } else {
                res.status(400).end("Unknown file type.");
            }
        } else {
            res.status(400).end("Missing parameters.");
        }
    }
}

module.exports = ContentController;
