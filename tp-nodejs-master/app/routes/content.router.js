'use strict';
// default.route.js
var multer = require("multer");
var express = require("express");
var http = require('http');
var fs = require('fs');
var contentController = require('../controllers/content.controller');

let CONFIG = JSON.parse(process.env.CONFIG);


var router = express.Router();
var multerMiddleware = multer({ "dest": "/tmp/" });
router.post("/contents", multerMiddleware.single("file"), contentController.create);
/**
 * Multer ajoute à l'objet `request` la propriété `file` qui contient plusieurs informations comme:
 *  - request.file.path : le chemin d'acces du fichier sur le serveur
 *  - request.file.originalname : le nom original du fichier
 *  - request.file.mimetype : le type mime
 *  - ...
 */

module.exports = router;

router.route('/contents')
    // retourne la liste des métadonnées de contenu de slides
    // disponibles sur le serveur ([content.id].meta.json)
    .get(contentController.list)
    // crée un nouveau contenu à partir du formulaire d'ajout de
    // contenu ('file', 'title', 'type', 'src')
    //.post(contentController.create);
router.route('/contents/:contentId')
    // retourne le contenu avec l'ID correspondant.
    .get(contentController.read)
    //.put(contentController.update)
    //.delete(contentController.delete);
router.param('contentId', function(req, res, next, id) {
    req.contentId = id;
    next();
});