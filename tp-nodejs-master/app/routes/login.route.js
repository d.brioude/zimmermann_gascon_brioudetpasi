'use strict';
// default.route.js
var express = require("express");
var request = require('request');
var router = express.Router();
module.exports = router;

function getLoginDetails(login, pwd, callback) {

    request.post('http://www.yoursite.com/formpage', {json: {login: login, pwd: pwd}},
        function (error, response, body) {
        console.log(body);
            if (!error && response.statusCode == 200) {
                callback(body);
            }
        }
    );
}

router.route('/login')
    .post(function(request, response){

        let str_body = "";
        request.on('data', function (chunk) {
            console.log(chunk);
            str_body += chunk;
        });
        request.on('end', function () {
            // {login: jdoe, pwd: jdoepwd}
            var body = JSON.parse(str_body);

            var login = body.login;
            var pwd = body.pwd;

            getLoginDetails(login, pwd, function(userDetails){

                // {login: jdoe, validAuth: true, role: ‘ADMIN’}
                console.log("login: " + userDetails.login);
                console.log("validAuth: " + userDetails.validAuth);
                console.log("role: " + userDetails.role);
                // TODO figure out how to implement the auth.
                //  We could consider to send a dated token to the user that would be required by the http srv and our
                //  sockets
            });
        })
    });

