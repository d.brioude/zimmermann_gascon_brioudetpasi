'use strict';
// default.route.js
var express = require("express");
var router = express.Router();
module.exports = router;

router.route('/api/')
    .get(function(request, response){
        response.send('hello api');
    })

router.route('/')
    .get(function(request, response){
        response.send('hello world');
    })
    /*
    .post(function(request, response){...})
    .put(function(request, response){...})
    .delete(function(request, response){...})
    .all(function(request, response){...})
    */
