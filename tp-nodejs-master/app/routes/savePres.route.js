'use strict';
// default.route.js
const express = require("express");
const fs = require('fs');
let CONFIG = JSON.parse(process.env.CONFIG);
let router = express.Router();
module.exports = router;

router.route('/savePres')
    .post(function(request, response){
        let str_body = "";
        request.on('data', function (chunk) {
            console.log(chunk);
            str_body += chunk;
        });
        request.on('end', function () {
            var body = JSON.parse(str_body);
            let presentation = body.newPres;
            let presentation_name = presentation.id + ".pres.json";

            fs.writeFile(CONFIG["presentationDirectory"] + "/" + presentation_name, presentation.toString(), function(err) {
                if(err) {
                    return console.log(err);
                }
                console.log("The file was saved!");
                response.json("The file was saved!");
            });
        })
    })
