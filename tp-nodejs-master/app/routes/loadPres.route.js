'use strict';
// default.route.js
var express = require("express");
var http = require('http');
var fs = require('fs');
var path = require('path');
let CONFIG = JSON.parse(process.env.CONFIG);


var router = express.Router();

module.exports = router;

router.route('/loadPres')
    .get(function(request, response){
        let presentations = {};
        console.log('request starting...');
        fs.readdir(CONFIG["presentationDirectory"], (err, files) => {
            let idx = 0;
            files.forEach(file_name => {
                if (file_name.split(".")[2] == "json") {
                    let file_path = CONFIG["presentationDirectory"] + "/" + file_name;
                    console.log(file_path);
                    fs.readFile(file_path, function(error, content) {
                        console.log("CONT: " + content);
                        content = JSON.parse(content);
                        presentations[content.id] = content;
                        idx += 1;
                        if (idx == files.length){
                            response.json(presentations);
                        }
                    });
                }
            });
        })
    })
