$(function () {

    function guid() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    };

// if user is running mozilla then use it's built-in WebSocket
    var connection = io.connect('http://127.0.0.1:1337');
//var connection = new WebSocket('http://127.0.0.1:1337');

    connection.on('connection', function (socket) {
        console.log("connected.");
        this.emit("data_comm", {
            "clientId": connection.id,
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349"
        });

        this.emit("slidEvent", {
            'event': "START",
            'cmdId': guid(),
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349",
            'slidId': ""});

        // c5c35356-1347-457d-af6b-e3d40862c1e0 s1

        this.emit("slidEvent", {
            'event': "NEXT",
            'cmdId': guid(),
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349",
            'slidId': "c5c35356-1347-457d-af6b-e3d40862c1e0"})

        // ba5bd952-2688-46b8-9b01-33723e1e0cbd s2

        this.emit("slidEvent", {
            'event': "PREV",
            'cmdId': guid(),
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349",
            'slidId': "ba5bd952-2688-46b8-9b01-33723e1e0cbd"});

        // c5c35356-1347-457d-af6b-e3d40862c1e0 s1

        this.emit("slidEvent", {
            'event': "PAUSE",
            'cmdId': guid(),
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349",
            'slidId': ""});

        // c5c35356-1347-457d-af6b-e3d40862c1e0 s1

    /*
        setTimeout(function(){
            console.log("NEXT IT");
            connection.emit("slidEvent", {
            'event': "NXT",
            'cmdId': guid(),
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349",
            'slidId': "ba5bd952-2688-46b8-9b01-33723e1e0cbd"});
        }, 4000);

        /*
        setTimeout(function(){
            console.log("NEXT IT");
            connection.emit("slidEvent", {
            'event': "NEXT",
            'cmdId': guid(),
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349",
            'slidId': ""});
        }, 8000);*/

        // Apparently timeout() is locking the script execution and thus disables event catching
        /*
        setTimeout(function(){
            console.log("PAUSE IT");
            connection.emit("slidEvent", {
            'event': "PAUSE",
            'cmdId': guid(),
            'presId': "efa0a79a-2f20-4e97-b0b7-71f824bfe349",
            'slidId': ""});
        }, 20000)  */
    });

    connection.on("success", function(data){
        console.log("OK. New presentation id is " + data.result.id);
    })

    connection.on("failure", function(data){
        console.log("An error happened server side." +
            "The command " + data.cmdType + " " +data.cmdId+" failed: " + data.result);
    })

});