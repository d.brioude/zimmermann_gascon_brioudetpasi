package fr.cpe.services;

import javax.ejb.Local;
import java.util.List;

@Local
public interface AuthService {
    public String login(String login, String pwd);
}