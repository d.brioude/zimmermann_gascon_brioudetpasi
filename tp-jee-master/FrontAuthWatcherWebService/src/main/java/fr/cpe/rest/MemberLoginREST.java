package fr.cpe.rest;

import fr.cpe.ejb.MessageReceiverSyncLocal;
import fr.cpe.ejb.MessageSenderLocal;
import fr.cpe.model.Credential;
import fr.cpe.model.UserModel;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/WatcherAuth")
@RequestScoped
public class MemberLoginREST {

    private static final long serialVersionUID = 1L;
    // private JmsSender sender;
    @EJB
    MessageSenderLocal sender;
    @EJB
    MessageReceiverSyncLocal receiver;

    private Logger log = Logger.getLogger(this.getClass().getName());

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public JsonObject doPost(Credential credential ) {
        sender.sendMessage(credential);
        UserModel userRet = receiver.receiveMessage();
        if(userRet != null){
            log.info(userRet.toString());
            JsonObject ret = Json.createObjectBuilder()
                    .add("login",userRet.getLogin())
                    .add("validAuth",true)
                    .add("role",userRet.getRole())
                    .build();
            return ret;
        }
        else{
            log.log(Level.WARNING,"Wrong user or password");
            JsonObject ret = Json.createObjectBuilder()
                    .add("login","")
                    .add("validAuth",false)
                    .add("role","")
                    .build();
            return ret;
        }

    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String hello(){
        return "Hello";
    }

}
