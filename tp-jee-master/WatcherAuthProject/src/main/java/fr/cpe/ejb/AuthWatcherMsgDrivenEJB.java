package fr.cpe.ejb;

import java.util.Date;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.cpe.model.Credential;
import fr.cpe.model.DataContainer;
import fr.cpe.model.UserModel;

@MessageDriven(
        activationConfig = {
                @ActivationConfigProperty(
                        propertyName = "destinationType",
                        propertyValue = "javax.jms.Topic"),
                @ActivationConfigProperty(
                        propertyName = "destination",
                        propertyValue = "java:/jms/queue/AuthTopic")
        })
public class AuthWatcherMsgDrivenEJB implements MessageListener {

    private DataContainer dataContainer;

    @EJB MessageSenderQueueLocal sender;

    @PersistenceContext
    private EntityManager em;

    public AuthWatcherMsgDrivenEJB() {
        dataContainer=new DataContainer();
    }

    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                System.out.println("Topic: I received a TextMessage at "
                        + new Date());
                TextMessage msg = (TextMessage) message;
                System.out.println("Message is : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                System.out.println("Topic: I received an ObjectMessage at "
                        + new Date());
                ObjectMessage msg = (ObjectMessage) message;
                if( msg.getObject() instanceof Credential){
                    Credential cred = (Credential) msg.getObject();
                    try {
                        UserModel user =
                                (UserModel) em.createQuery
                                        ("select u from UserModel u where u.pwd = :pwd AND u.login = :login")
                                .setParameter("pwd", cred.getPwd())
                                .setParameter("login", cred.getLogin())
                                .getSingleResult();

                        System.out.println("User Details: ");
                        System.out.println("login:" + user.getLogin());
                        System.out.println("pwd:" + user.getPwd());
                  /*  Role currentTestRole=dataContainer.checkUser(user);
                    if( Role.NONE==currentTestRole){
                        sender.sendMessage(user);
                    }else{
                        user.setRole(currentTestRole);
                        sender.sendMessage(user);
                    }*/
                        sender.sendMessage(user);
                    }
                    catch(Exception e){
                        sender.sendMessage((UserModel) null);
                    }
                }
            } else {
                System.out.println("Not valid message for this Queue MDB");
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}