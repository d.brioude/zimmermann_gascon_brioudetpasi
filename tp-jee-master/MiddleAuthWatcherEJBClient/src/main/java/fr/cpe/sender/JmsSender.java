package fr.cpe.sender;
import javax.ejb.Local;

@Local
public interface JmsSender {
    void sendResponse();
}
