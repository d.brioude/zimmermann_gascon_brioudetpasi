package fr.cpe.model;

import javax.persistence.*;

@Entity
@Table(name = "User")
public class UserModel implements java.io.Serializable{

    @Id
    @Column(name = "IdUser")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "surnameName")
    private String surnameName;

    @Column(name = "pwd")
    private String pwd;

    @Column(name = "role")
    private String role;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurnameName() {
        return surnameName;
    }

    public void setSurnameName(String surnameName) {
        this.surnameName = surnameName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserModel(){
    }

    public UserModel(String login, String pwd, String lastName, String surnameName, String role) {
        this.login = login;
        this.lastName = lastName;
        this.surnameName = surnameName;
        this.pwd = pwd;
        this.role = role;
    }

    public UserModel(Credential cred, String lastName, String surnameName, String role) {
        this.login = cred.getLogin();
        this.lastName = lastName;
        this.surnameName = surnameName;
        this.pwd = cred.getPwd();
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "login='" + login + '\'' +
                ", lastName='" + lastName + '\'' +
                ", surnameName='" + surnameName + '\'' +
                ", pwd='" + pwd + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
