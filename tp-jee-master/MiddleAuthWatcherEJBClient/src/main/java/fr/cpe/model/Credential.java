package fr.cpe.model;

import java.io.Serializable;

public class Credential implements Serializable {

    private String login;
    private String pwd;

    public String getLogin() {
        return login;
    }

    public void setLogin(String loging) {
        this.login = loging;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
