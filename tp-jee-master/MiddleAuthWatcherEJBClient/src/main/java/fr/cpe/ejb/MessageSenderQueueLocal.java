package fr.cpe.ejb;

import fr.cpe.model.UserModel;

import javax.ejb.Local;

@Local
public interface MessageSenderQueueLocal {
    void sendMessage(UserModel user);
    void sendMessage(String message);
}
