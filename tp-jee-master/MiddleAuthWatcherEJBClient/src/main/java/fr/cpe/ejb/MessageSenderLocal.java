package fr.cpe.ejb;

import fr.cpe.model.Credential;
import fr.cpe.model.UserModel;

import javax.ejb.Local;

@Local
public interface MessageSenderLocal {

     void sendMessage(String message);
     void sendMessage(Credential cred);
}
