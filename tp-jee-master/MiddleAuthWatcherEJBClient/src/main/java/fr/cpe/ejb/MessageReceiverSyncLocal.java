package fr.cpe.ejb;

import fr.cpe.model.UserModel;

import javax.ejb.Local;

@Local
public interface MessageReceiverSyncLocal {
    UserModel receiveMessage();
}
