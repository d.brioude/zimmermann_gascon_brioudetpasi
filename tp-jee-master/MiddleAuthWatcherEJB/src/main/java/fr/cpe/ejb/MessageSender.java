package fr.cpe.ejb;

import fr.cpe.model.Credential;
import fr.cpe.model.UserModel;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Topic;

@Stateless
@LocalBean
public class MessageSender implements MessageSenderLocal {
    @Resource(name = "java:/jms/queue/AuthTopic")
    private Topic topic;

    @Inject
    private JMSContext context;

    public void sendMessage(String message) {
    }
    public void sendMessage(Credential cred) {
        context.createProducer().send(topic,cred);
    }

}
