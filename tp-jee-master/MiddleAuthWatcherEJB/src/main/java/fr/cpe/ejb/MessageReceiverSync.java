package fr.cpe.ejb;

import fr.cpe.model.UserModel;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

@Stateless
public class MessageReceiverSync implements MessageReceiverSyncLocal{

    @Resource(name = "java:/jms/queue/AuthQueue")
    private Queue queue;

    @Inject
    private JMSContext context;

    private JMSConsumer consumer;

    @Override
    public UserModel receiveMessage() {
        consumer = context.createConsumer(queue);

        int count = 0;
        while (true) {
            Message m = consumer.receive(1000);
            if (m != null) {
                if (m instanceof ObjectMessage) {
                    try {
                        System.out.println("Reading message: " + m.getBody(UserModel.class));
                        return m.getBody(UserModel.class);
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                    count += 1;
                } else {
                    break;
                }
            }
        }
        System.out.println("Messages received: " + count);
        return null;
    }
}
